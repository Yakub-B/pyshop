from rest_framework import serializers
from shop.models import Product, Category


class ProductDetailSerializer(serializers.ModelSerializer):
    category_queryset = Category.objects.all()
    category = serializers.SlugRelatedField(slug_field='name', queryset=category_queryset)

    class Meta:
        model = Product
        fields = '__all__'


class ProductListSerializer(serializers.ModelSerializer):
    category = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'price', 'available')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
