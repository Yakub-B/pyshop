from django.urls import path
from . import views

app_name = 'productsAPI'

urlpatterns = [
    path('category-list/', views.CategoryListAPIView.as_view()),
    path('category/create', views.CategoryCreateAPIView.as_view()),
    path('category/<int:pk>', views.CategoryDetailAPIView.as_view()),
    path('product-list/', views.ProductListAPIView.as_view()),
    path('product/create', views.ProductCreateAPIView.as_view()),
    path('product/<int:pk>', views.ProductDetailAPIView.as_view())
]
