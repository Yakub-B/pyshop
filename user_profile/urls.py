from django.contrib.auth.views import LogoutView
from django.urls import path
from . import views

app_name = 'user_profile'


urlpatterns = [
    path('login/', views.UserLoginView.as_view(), name='user_login'),
    path('logout/', LogoutView.as_view(), name='user_logout'),
    path('signup/', views.UserRegistrationView.as_view(), name='user_registration'),
    path('profile/update/', views.UserProfileView.as_view(), name='profile_update'),
    path('profile/', views.UserAccountView.as_view(), name='user_profile')
]
