from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from user_profile.models import UserProfile


class LoginForm(forms.Form):
    """
    Simple login form
    """
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class AccountUpdateForm(forms.ModelForm):
    date_joined = forms.DateField(disabled=True)

    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email', 'date_joined']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ('user',)
