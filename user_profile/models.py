from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    """
    User`s profile class. Have OneToOne relation with default user model.
    Stores client information for making orders.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    address = models.CharField(max_length=250)
    postal_code = models.CharField(max_length=20)
    city = models.CharField(max_length=100)
