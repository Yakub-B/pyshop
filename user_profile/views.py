from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView

from .forms import LoginForm, SignUpForm, ProfileUpdateForm, AccountUpdateForm
from django.shortcuts import render, redirect
from django.views.generic.base import View
from .models import UserProfile


class UserLoginView(View):
    """
    View for authentication and logging users
    """
    def get(self, request):
        """
        For get method view check if user is already logged in
        and if not - renders login form
        """
        form = LoginForm()
        if request.user.is_authenticated:
            return render(request, 'registration/logged_in.html')
        return render(request, 'registration/login.html', context={'form': form})

    def post(self, request):
        """
        For post method view tries to authenticate user using credentials
        from LoginForm and if authentication goes well - login user
        else if authentication fails - renders LoginForm with wrong
        credentials message
        """
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                login(request, user)
                return redirect('shop:product_list')
            else:
                form = LoginForm()
                return render(request, 'registration/login.html',
                              context={'form': form, 'error': 'Wrong username or password.'})


class UserRegistrationView(View):
    def get(self, request):
        form = SignUpForm()
        return render(request, 'registration/registration_page.html', context={'form': form})

    def post(self, request):
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('shop:product_list')


class UserAccountView(LoginRequiredMixin, View):

    # template_name = 'registration/profile.html'
    def get(self, request):
        user_id = request.user.pk
        user = get_user_model().objects.get(pk=user_id)
        account_form = AccountUpdateForm(instance=user)
        profile, _ = UserProfile.objects.get_or_create(user_id=user_id)
        profile_form = ProfileUpdateForm(instance=profile)
        return render(request, 'registration/profile.html', context={
            'form': account_form,
            'profile_form': profile_form
        })

    def post(self, request):
        user_id = request.user.pk
        user = get_user_model().objects.get(pk=user_id)
        form = AccountUpdateForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('user_profile:user_profile')


class UserProfileView(LoginRequiredMixin, View):
    def post(self, request):
        user_id = request.user.pk
        profile, _ = UserProfile.objects.get_or_create(user_id=user_id)
        form = ProfileUpdateForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return redirect('user_profile:user_profile')


# ToDo: make low-level profile view and forms
