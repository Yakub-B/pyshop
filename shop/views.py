from django.views.generic import DetailView, ListView
from cart.forms import CartAddProductForm
from .models import Product, Category


class ProductListView(ListView):
    """
    Class-based view for store's main page
    Show all available products if no category selected
    or all products for selected category
    """
    template_name = 'product/list.html'
    context_object_name = 'products'

    def get_queryset(self):
        if 'category_slug' in self.kwargs:
            category_slug = self.kwargs['category_slug']
            return Product.objects.filter(category__slug=category_slug)
        else:
            return Product.objects.filter(available=True)

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        if 'category_slug' in self.kwargs:
            category_slug = self.kwargs['category_slug']
            context['category'] = Category.objects.get(slug=category_slug)
        context['categories'] = Category.objects.all()
        return context

    paginate_by = 3


class ProductDetailView(DetailView):
    """
    Class-based view for product detail page
    """
    query_pk_and_slug = True
    context_object_name = 'product'
    template_name = 'product/detail.html'

    def get_queryset(self):
        #  getting only available products
        return Product.objects.filter(available=True)

    def get_context_data(self, **kwargs):
        #  adding CartAddProductForm form to template context
        cart_product_form = CartAddProductForm()
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['cart_product_form'] = cart_product_form
        return context
