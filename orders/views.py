from django.shortcuts import render
from django.views.generic.base import View

from user_profile.models import UserProfile
from .models import OrderItem
from .forms import OrderCreateForm
from cart.services.cart import Cart


class CreateOrderView(View):

    def get(self, request):
        cart = Cart(request)
        if request.user.is_authenticated:
            user = request.user
            profile, _ = UserProfile.objects.get_or_create(user_id=user.id)
            data_from_user_profile = {
                'client_first_name': user.first_name,
                'client_last_name': user.last_name,
                'client_email': user.email,
                'address': profile.address,
                'postal_code': profile.postal_code,
                'city': profile.city
            }
            form = OrderCreateForm(data_from_user_profile)
        else:
            form = OrderCreateForm()
        return render(request, 'order/create.html', {'cart': cart, 'form': form})

    def post(self, request):
        cart = Cart(request)
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            for item in cart:
                OrderItem.objects.create(
                    order=order, product=item['product'],
                    price=item['price'], quantity=item['quantity']
                )
            cart.clear()
            return render(request, 'order/created.html', {'order': order})
        else:
            form = OrderCreateForm()
            return render(request, 'order/create.html', {'cart': cart, 'form': form})
