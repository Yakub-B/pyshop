from django import forms
from .models import Order


class OrderCreateForm(forms.ModelForm):
    """Form for creating orders"""
    class Meta:
        model = Order
        fields = [
            'client_first_name', 'client_last_name', 'client_email',
            'address', 'postal_code', 'city'
        ]
