from decimal import Decimal
from django.conf import settings
from shop.models import Product


class Cart:
    """
    This is the Cart class, which will be responsible for working with shopping carts.
    """
    def __init__(self, request):
        self.session = request.session
        # getting cart from the session or if there is none, create it
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def add(self, product, quantity=1, update_quantity=False):
        """
        Adding an item to the cart or updating its quantity
        :param product: Product model object to add or update
        :param quantity: Optional number of objects, default 1
        :param update_quantity: A Boolean value that indicates whether to replace the value of the quantity
         of products with a new one (True) or add it to the existing one (False).
        """
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {'quantity': 0, 'price': str(product.price)}
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        self.save()

    def save(self):
        # Mark the session as modified
        self.session.modified = True

    def remove(self, product):
        """Removing product from cart"""
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def __iter__(self):
        product_ids = self.cart.keys()

        products = Product.objects.filter(id__in=product_ids)

        cart = self.cart.copy()
        for product in products:
            cart[str(product.id)]['product'] = product
        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        """Returns the total number of items in the cart."""
        return sum(item['quantity'] for item in self.cart.values())

    def get_total_price(self):
        """Returns total price of all products in the cart, given their quantity"""
        return sum(
            Decimal(item['price']) * item['quantity'] for item in self.cart.values()
        )

    def clear(self):
        # Empty the cart.
        del self.session[settings.CART_SESSION_ID]
        self.save()
