from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from shop.models import Product
from cart.services.cart import Cart
from .forms import CartAddProductForm


@require_POST
def cart_add(request, product_id):
    """
    This view implements the method of adding product to cart
    """
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        data = form.cleaned_data
        cart.add(
            product=product,
            quantity=data['quantity'],
            update_quantity=data['update']
        )
    return redirect('cart:cart_detail')


def cart_remove(request, product_id):
    """
    This view implements the method of removing product from cart
    """
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')


def cart_detail(request):
    """
    This view renders detail cart page
    """
    cart = Cart(request)
    # adding to all products in cart CartAddProductForm in order to be able
    # update to update their quantity from detail cart page
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
            initial={'quantity': item['quantity'], 'update': True},
        )
    return render(request, 'cart/detail.html', {'cart': cart})
